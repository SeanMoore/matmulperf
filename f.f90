program main
   use, intrinsic :: iso_c_binding, only: sp=>c_float, dp=>c_double
   implicit none

   integer, parameter :: DIM = 2000
   integer, parameter :: AR = DIM
   integer, parameter :: AC = DIM
   integer, parameter :: BR = AC
   integer, parameter :: BC = DIM
   real(sp) :: a(AR,AC)
   real(sp) :: b(BR,BC)
   real(sp) :: c(AR,BC)
   integer :: i
   integer :: j
   real(sp) :: sum

   do concurrent (i=1:AR)
      do concurrent (j=1:AC)
         a(i, j) = (i-1)*(j-1)
      end do
   end do

   do concurrent (i=1:BR)
      do concurrent (j=1:BC)
         b(i, j) = (i-1)*(j-1)
      end do
   end do

   c = matmul(a, b)

   sum = 0
   do i=1,AR
      do j=1,BC
         sum = sum + c(i,j)
      end do
   end do
   print *, sum
end program main
