#!/bin/bash

make -j $(nproc)

function run() {
    echo $1
    time timeout 60s bin/$1.elf

    rval=$?
    if [ $rval -ne 0 ]
    then
        echo '***************'
        echo '*** TIMEOUT ***'
        echo '***************'
    fi
    echo
}

# FAST
run rv_fast
run cf_fast
run f_fast
run r_rayon_fast
run c_fast
run cpp_fast
run stl_fast
run r_fast

# SLOW
run cf_slow
run f_slow
run r_rayon_slow
run c_slow
run cpp_slow
run stl_slow
run r_slow

# make clean
