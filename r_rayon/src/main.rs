use itertools::Itertools;
use rayon::prelude::*;

const DIMENSION: usize = 2000;

const AR: usize = DIMENSION;
const AC: usize = DIMENSION;
const BR: usize = AC;
const BC: usize = DIMENSION;

type Value = f32;

fn main() {
    let matrix_a: Vec<Vec<Value>> = (0..AR)
        .map(|r| (0..AC).map(|c| (r * c) as Value).collect())
        .collect();
    let matrix_b: Vec<Vec<Value>> = (0..BC)
        .map(|r| (0..BR).map(|c| (r * c) as Value).collect())
        .collect();
    let matrix_c: Vec<Vec<Value>> = (0..AR)
        .into_par_iter()
        .map(|r| {
            let matrix_a_c = &matrix_a[r];
            (0..BC)
                .into_par_iter()
                .map(|c| {
                    let matrix_b_r = &matrix_b[c];
                    (0..AC).map(|i| matrix_a_c[i] * matrix_b_r[i]).sum()
                })
                .collect()
        })
        .collect();
    // We want to have a running accumulator over the whole array.
    // Don't add up across rows then add together columns.
    // This keeps in line with the other implementations.
    let sum: Value = (0..BC)
        .cartesian_product(0..AR)
        .map(|(r, c)| matrix_c[r][c])
        .sum();

    println!("{}", sum);
}
