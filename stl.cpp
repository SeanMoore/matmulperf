#include <array>
#include <cstdio>
#include <cstdlib>
#include <execution>
#include <random>

#define DIMENSION 2000

#define AR DIMENSION
#define AC DIMENSION
#define BR AC
#define BC DIMENSION

typedef float Value;

template <typename T, int i, int j>
using array2d = std::array<std::array<Value, j>, i>;

array2d<Value, AR, AC> a;
array2d<Value, BR, BC> b;
array2d<Value, AR, BC> c;

std::array<int, DIMENSION> counter;

int main() {
    for( int i = 0; i < DIMENSION; ++i ) {
        counter[i] = i;
    }

    std::for_each( std::execution::unseq, counter.begin(), counter.end(), [&]( int i ) {
        std::for_each( std::execution::unseq, counter.begin(), counter.end(), [&]( int j ) {
            a[i][j] = i * j;
        } );
    } );

    std::for_each( std::execution::unseq, counter.begin(), counter.end(), [&]( int j ) {
        std::for_each( std::execution::unseq, counter.begin(), counter.end(), [&]( int i ) {
            b[j][i] = i * j;
        } );
    } );

    std::for_each( std::execution::unseq, counter.begin(), counter.end(), [&]( int i ) {
        std::for_each( std::execution::unseq, counter.begin(), counter.end(), [&]( int j ) {
            c[i][j] = std::transform_reduce(
                std::execution::unseq, counter.begin(), counter.end(), 0.0,
                [&]( Value l, Value r ) {
                    return l + r;
                },
                [&]( int k ) {
                    return a[i][k] * b[j][k];
                } );
        } );
    } );

    Value sum = 0.0;
    for( int i = 0; i < AR; ++i ) {
        for( int j = 0; j < BC; ++j ) {
            sum += c[i][j];
        }
    }
    printf( "%f\n", sum );
}