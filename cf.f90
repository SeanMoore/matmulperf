subroutine fmatmul(i, j, k, a, b, c)
   use, intrinsic :: iso_c_binding, only: sp=>c_float, dp=>c_double, int=>c_int
   implicit none

   ! Note that when called from C the indices are reversed.

   integer(int), intent(in) :: i, j, k
   real(sp), intent(in), dimension(k,i) :: a
   real(sp), intent(in), dimension(j,k) :: b
   real(sp), intent(out), dimension(j,i) :: c

   c = matmul(b, a)
end subroutine fmatmul
