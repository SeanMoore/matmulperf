use std::sync::Arc;
use vulkano::{
    device::{
        physical::{PhysicalDevice, PhysicalDeviceType},
        Device, DeviceCreateInfo, DeviceExtensions, Queue, QueueCreateInfo,
    },
    instance::{Instance, InstanceCreateInfo},
};

use crate::error::*;

pub(crate) fn new() -> Result<(Arc<Device>, Arc<Queue>)> {
    let instance = match Instance::new(InstanceCreateInfo {
        enumerate_portability: true,
        ..Default::default()
    }) {
        Ok(instance) => Ok(instance),
        Err(e) => Err(Error::VulkanoInstance(e)),
    }?;

    let device_extensions = DeviceExtensions {
        khr_storage_buffer_storage_class: true,
        ..DeviceExtensions::none()
    };
    let (physical_device, queue_family) = match PhysicalDevice::enumerate(&instance)
        .filter(|&p| p.supported_extensions().is_superset_of(&device_extensions))
        .filter_map(|p| {
            p.queue_families()
                .find(|&q| q.supports_compute())
                .map(|q| (p, q))
        })
        .min_by_key(|(p, _)| match p.properties().device_type {
            PhysicalDeviceType::DiscreteGpu => 0,
            PhysicalDeviceType::IntegratedGpu => 1,
            PhysicalDeviceType::VirtualGpu => 2,
            PhysicalDeviceType::Cpu => 3,
            PhysicalDeviceType::Other => 4,
        }) {
        Some(v) => Ok(v),
        None => Err(Error::VulkanoDeviceMissing),
    }?;

    let (device, mut queues) = match Device::new(
        physical_device,
        DeviceCreateInfo {
            enabled_extensions: device_extensions,
            queue_create_infos: vec![QueueCreateInfo::family(queue_family)],
            ..Default::default()
        },
    ) {
        Ok(v) => Ok(v),
        Err(e) => Err(Error::VulkanoDeviceCreate(e)),
    }?;

    let queue = match queues.next() {
        Some(queue) => Ok(queue),
        None => Err(Error::VulkanoQueue),
    }?;

    Ok((device, queue))
}
