use thiserror::Error;

pub(crate) type Result<T> = std::result::Result<T, Error>;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Failed to create a vulkano instance {0}.")]
    VulkanoInstance(vulkano::instance::InstanceCreationError),
    #[error("Failed to find a suitable vulkano device.")]
    VulkanoDeviceMissing,
    #[error("Failed to create a vulkano device {0}.")]
    VulkanoDeviceCreate(vulkano::device::DeviceCreationError),
    #[error("Failed to retrieve a matching vulkano queue.")]
    VulkanoQueue,
    #[error("Failed to create vulkano compute pipeline {0}.")]
    VulkanoComputePipelineCreation(vulkano::pipeline::compute::ComputePipelineCreationError),
    #[error("Failed to create vulkano shader {0}.")]
    VulkanoShaderCreation(vulkano::shader::ShaderCreationError),
    #[error("Failed to find vulkano shader entry point.")]
    VulkanoShaderEntryPoint,
    #[error("Failed to allocate vulkano device memory {0}.")]
    VulkanoDeviceMemoryAllocation(vulkano::memory::DeviceMemoryAllocationError),
    #[error("Failed to retrieve shader descriptor set layout.")]
    VulkanoDescriptorSetLayout,
    #[error("Failed to create descriptor set {0}.")]
    VulkanoDescriptorSetCreate(vulkano::descriptor_set::DescriptorSetCreationError),
    #[error("Failed to begin building command buffer {0}.")]
    VulkanoCommandBufferBegin(vulkano::command_buffer::CommandBufferBeginError),
    #[error("Failed to dispatch while building command buffer {0}.")]
    VulkanoCommandBufferDispatch(vulkano::command_buffer::DispatchError),
    #[error("Failed to build command buffer {0}.")]
    VulkanoCommandBufferBuild(vulkano::command_buffer::BuildError),
    #[error("Failed to execute command buffer {0}.")]
    VulkanoCommandBufferExec(vulkano::command_buffer::CommandBufferExecError),
    #[error("Failed to flush command buffer {0}.")]
    VulkanoCommandBufferFlush(vulkano::sync::FlushError),
    #[error("Failed to wait for command buffer flush {0}.")]
    VulkanoCommandBufferWait(vulkano::sync::FlushError),
    #[error("Failed to acquite read lock {0}.")]
    VulkanoReadLock(vulkano::buffer::cpu_access::ReadLockError),
}
