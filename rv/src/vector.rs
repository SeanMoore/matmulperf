use bytemuck::{Pod, Zeroable};
use std::sync::Arc;

#[repr(C)]
#[derive(Default, Copy, Clone, Zeroable, Pod)]
pub(crate) struct Layout {
    pub(crate) stride: u32,
    pub(crate) size: u32,
}

#[repr(C)]
#[derive(Default, Copy, Clone, Zeroable, Pod)]
pub(crate) struct Layout1 {
    pub(crate) array: [Layout; 1],
}

#[repr(C)]
#[derive(Default, Copy, Clone, Zeroable, Pod)]
pub(crate) struct Layout2 {
    pub(crate) array: [Layout; 2],
}

#[repr(C)]
#[derive(Default, Copy, Clone, Zeroable, Pod)]
pub(crate) struct Layout3 {
    pub(crate) array: [Layout; 3],
}

// #[derive(Clone)]
// pub(crate) struct Vector<const R: u32, BufferLayout, BufferData> {
//     pub(crate) layout: Layout1,
//     pub(crate) buffer_layout: Arc<BufferLayout>,
//     pub(crate) buffer_data: Arc<BufferData>,
// }

pub(crate) struct Matrix<const R: u32, const C: u32, BufferLayout, BufferData> {
    pub(crate) layout: Layout2,
    pub(crate) buffer_layout: Arc<BufferLayout>,
    pub(crate) buffer_data: Arc<BufferData>,
}

// #[derive(Clone)]
// pub(crate) struct Tensor<const R: u32, const C: u32, const A: u32, BufferLayout, BufferData> {
//     pub(crate) layout: Layout3,
//     pub(crate) buffer_layout: Arc<BufferLayout>,
//     pub(crate) buffer_data: Arc<BufferData>,
// }

impl<const R: u32, const C: u32, BufferLayout, BufferData> Clone
    for Matrix<R, C, BufferLayout, BufferData>
{
    fn clone(&self) -> Self {
        Self {
            layout: self.layout.clone(),
            buffer_layout: self.buffer_layout.clone(),
            buffer_data: self.buffer_data.clone(),
        }
    }
}
