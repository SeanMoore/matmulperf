#![warn(unused_extern_crates)]

use vulkano::{
    buffer::{BufferUsage, CpuAccessibleBuffer},
    memory::pool::{PotentialDedicatedAllocation, StdMemoryPoolAlloc},
};

mod device_queue;
mod error;
mod matrix_multiply;
mod vector;

use crate::error::*;

type Value = f32;
const DIMENSION: u32 = 2000;
const AR: u32 = DIMENSION;
const AC: u32 = DIMENSION;
const BR: u32 = AC;
const BC: u32 = DIMENSION;

fn _print_matrix<const R: u32, const C: u32>(
    matrix: vector::Matrix<
        R,
        C,
        CpuAccessibleBuffer<vector::Layout2>,
        CpuAccessibleBuffer<[Value], PotentialDedicatedAllocation<StdMemoryPoolAlloc>>,
    >,
) -> Result<()> {
    let lock = match matrix.buffer_data.read() {
        Ok(v) => Ok(v),
        Err(e) => Err(Error::VulkanoReadLock(e)),
    }?;
    let data = lock.to_vec();
    for r in 0..matrix.layout.array[1].size - 1 {
        for c in 0..matrix.layout.array[0].size - 1 {
            let id = r * matrix.layout.array[1].stride + c * matrix.layout.array[0].stride;
            print!("{} ", data[id as usize]);
        }
        println!();
    }
    Ok(())
}

fn main() -> Result<()> {
    let (device, queue) = device_queue::new()?;

    let buffer_usage_storage = BufferUsage {
        storage_buffer: true,
        ..BufferUsage::none()
    };

    let matrix_a = {
        let layout = vector::Layout2 {
            array: [
                vector::Layout {
                    stride: 1,
                    size: AC,
                },
                vector::Layout {
                    stride: AC,
                    size: AR,
                },
            ],
        };
        let buffer_layout = match CpuAccessibleBuffer::from_data(
            device.clone(),
            buffer_usage_storage.clone(),
            false,
            layout,
        ) {
            Ok(v) => Ok(v),
            Err(e) => Err(Error::VulkanoDeviceMemoryAllocation(e)),
        }?;

        let data_iter = (0..(AR * AC)).map(|n| {
            let r = n / DIMENSION;
            let c = n % DIMENSION;
            (r * c) as Value
        });
        let buffer_data = match CpuAccessibleBuffer::from_iter(
            device.clone(),
            buffer_usage_storage.clone(),
            false,
            data_iter,
        ) {
            Ok(v) => Ok(v),
            Err(e) => Err(Error::VulkanoDeviceMemoryAllocation(e)),
        }?;

        vector::Matrix {
            layout,
            buffer_layout,
            buffer_data,
        }
    };

    let matrix_b = {
        let layout = vector::Layout2 {
            array: [
                vector::Layout {
                    stride: BR,
                    size: BC,
                },
                vector::Layout {
                    stride: 1,
                    size: BR,
                },
            ],
        };
        let buffer_layout = match CpuAccessibleBuffer::from_data(
            device.clone(),
            buffer_usage_storage.clone(),
            false,
            layout,
        ) {
            Ok(v) => Ok(v),
            Err(e) => Err(Error::VulkanoDeviceMemoryAllocation(e)),
        }?;

        let data_iter = (0..(BR * BC)).map(|n| {
            let r = n / DIMENSION;
            let c = n % DIMENSION;
            (r * c) as Value
        });
        let buffer_data = match CpuAccessibleBuffer::from_iter(
            device.clone(),
            buffer_usage_storage.clone(),
            false,
            data_iter,
        ) {
            Ok(v) => Ok(v),
            Err(e) => Err(Error::VulkanoDeviceMemoryAllocation(e)),
        }?;

        vector::Matrix {
            layout,
            buffer_layout,
            buffer_data,
        }
    };

    let mmv = matrix_multiply::MatrixMultiply::<AR, AC, BC, Value>::new(
        device,
        queue,
        matrix_a.clone(),
        matrix_b.clone(),
    )?;
    let result = mmv.execute()?;
    let lock = match result.buffer_data.read() {
        Ok(v) => Ok(v),
        Err(e) => Err(Error::VulkanoReadLock(e)),
    }?;
    let data = lock.to_vec();

    let sum: Value = data.iter().sum();
    println!("{:e}", sum);

    Ok(())
}
