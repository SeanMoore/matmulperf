use std::sync::Arc;
use vulkano::{
    buffer::{BufferContents, BufferUsage, CpuAccessibleBuffer},
    command_buffer::{AutoCommandBufferBuilder, CommandBufferUsage, PrimaryAutoCommandBuffer},
    descriptor_set::{PersistentDescriptorSet, WriteDescriptorSet},
    device::{Device, Queue},
    pipeline::{ComputePipeline, Pipeline, PipelineBindPoint},
    sync::{self, GpuFuture},
};

use crate::error::*;
use crate::vector;

pub(crate) struct MatrixMultiply<const R: u32, const N: u32, const C: u32, T>
where
    [T]: BufferContents,
{
    device: Arc<Device>,
    queue: Arc<Queue>,
    command_buffer: PrimaryAutoCommandBuffer,
    matrix_c: vector::Matrix<R, C, CpuAccessibleBuffer<vector::Layout2>, CpuAccessibleBuffer<[T]>>,
}

impl<const R: u32, const N: u32, const C: u32, T> MatrixMultiply<R, N, C, T>
where
    [T]: BufferContents,
    T: Clone + num::Zero,
{
    pub(crate) fn new(
        device: Arc<Device>,
        queue: Arc<Queue>,
        matrix_a: vector::Matrix<
            R,
            N,
            CpuAccessibleBuffer<vector::Layout2>,
            CpuAccessibleBuffer<[T]>,
        >,
        matrix_b: vector::Matrix<
            N,
            C,
            CpuAccessibleBuffer<vector::Layout2>,
            CpuAccessibleBuffer<[T]>,
        >,
    ) -> Result<Self> {
        mod cs {
            vulkano_shaders::shader! {
                ty: "compute",
                path:"compute/matrix_multiply.glsl",
            }
        }
        let shader = match cs::load(device.clone()) {
            Ok(v) => Ok(v),
            Err(e) => Err(Error::VulkanoShaderCreation(e)),
        }?;
        let pipeline = match ComputePipeline::new(
            device.clone(),
            match shader.entry_point("main") {
                Some(v) => Ok(v),
                None => Err(Error::VulkanoShaderEntryPoint),
            }?,
            &(),
            None,
            |_| {},
        ) {
            Ok(v) => Ok(v),
            Err(e) => Err(Error::VulkanoComputePipelineCreation(e)),
        }?;

        let buffer_usage_storage = BufferUsage {
            storage_buffer: true,
            ..BufferUsage::none()
        };
        let matrix_c = {
            let layout = vector::Layout2 {
                array: [
                    vector::Layout { stride: 1, size: C },
                    vector::Layout { stride: C, size: R },
                ],
            };
            let buffer_layout = match CpuAccessibleBuffer::from_data(
                device.clone(),
                buffer_usage_storage.clone(),
                false,
                layout,
            ) {
                Ok(v) => Ok(v),
                Err(e) => Err(Error::VulkanoDeviceMemoryAllocation(e)),
            }?;

            let data_iter = (0..(R * C)).map(|_| T::zero());
            let buffer_data = match CpuAccessibleBuffer::from_iter(
                device.clone(),
                BufferUsage {
                    storage_buffer: true,
                    ..BufferUsage::none()
                },
                false,
                data_iter,
            ) {
                Ok(v) => Ok(v),
                Err(e) => Err(Error::VulkanoDeviceMemoryAllocation(e)),
            }?;

            vector::Matrix {
                layout,
                buffer_layout,
                buffer_data,
            }
        };

        let layout = match pipeline.layout().set_layouts().get(0) {
            Some(v) => Ok(v),
            None => Err(Error::VulkanoDescriptorSetLayout),
        }?;
        let set = match PersistentDescriptorSet::new(
            layout.clone(),
            [
                WriteDescriptorSet::buffer(0, matrix_a.buffer_data.clone()),
                WriteDescriptorSet::buffer(1, matrix_b.buffer_data.clone()),
                WriteDescriptorSet::buffer(2, matrix_c.buffer_data.clone()),
                WriteDescriptorSet::buffer(3, matrix_a.buffer_layout.clone()),
                WriteDescriptorSet::buffer(4, matrix_b.buffer_layout.clone()),
                WriteDescriptorSet::buffer(5, matrix_c.buffer_layout.clone()),
            ],
        ) {
            Ok(v) => Ok(v),
            Err(e) => Err(Error::VulkanoDescriptorSetCreate(e)),
        }?;

        let mut command_buffer_builder = match AutoCommandBufferBuilder::primary(
            device.clone(),
            queue.family(),
            CommandBufferUsage::MultipleSubmit, // Note that `MultipleSubmit` prevents simultaneous use.
        ) {
            Ok(v) => Ok(v),
            Err(e) => Err(Error::VulkanoCommandBufferBegin(e)),
        }?;

        let work_group_x_width = 16;
        let work_group_y_width = 16;
        let work_group_x_count =
            (C / work_group_x_width) + if 0 == C % work_group_x_width { 0 } else { 1 };
        let work_group_y_count =
            (R / work_group_y_width) + if 0 == R % work_group_y_width { 0 } else { 1 };

        match command_buffer_builder
            .bind_pipeline_compute(pipeline.clone())
            .bind_descriptor_sets(
                PipelineBindPoint::Compute,
                pipeline.layout().clone(),
                0,
                set.clone(),
            )
            .dispatch([work_group_x_count as u32, work_group_y_count as u32, 1])
        {
            Ok(v) => Ok(v),
            Err(e) => Err(Error::VulkanoCommandBufferDispatch(e)),
        }?; // Drop the mutable reference on the floor.

        let command_buffer = match command_buffer_builder.build() {
            Ok(v) => Ok(v),
            Err(e) => Err(Error::VulkanoCommandBufferBuild(e)),
        }?;

        Ok(Self {
            device,
            queue,
            command_buffer,
            matrix_c,
        })
    }

    pub(crate) fn execute(
        self,
    ) -> Result<vector::Matrix<R, C, CpuAccessibleBuffer<vector::Layout2>, CpuAccessibleBuffer<[T]>>>
    {
        match match match sync::now(self.device.clone())
            .then_execute(self.queue.clone(), self.command_buffer)
        {
            Ok(v) => Ok(v),
            Err(e) => Err(Error::VulkanoCommandBufferExec(e)),
        }?
        .then_signal_fence_and_flush()
        {
            Ok(v) => Ok(v),
            Err(e) => Err(Error::VulkanoCommandBufferFlush(e)),
        }?
        .wait(None)
        {
            Ok(v) => Ok(v),
            Err(e) => Err(Error::VulkanoCommandBufferWait(e)),
        }?;

        Ok(self.matrix_c)
    }
}
