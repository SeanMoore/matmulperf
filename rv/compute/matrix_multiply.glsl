#version 450

layout(local_size_x = 16, local_size_y = 16, local_size_z = 1) in;

struct Layout {
  uint stride;
  uint size;
};

layout(set = 0, binding = 0) buffer MatrixA { float data[]; }
matrix_a;
layout(set = 0, binding = 1) buffer MatrixB { float data[]; }
matrix_b;
layout(set = 0, binding = 2) buffer MatrixC { float data[]; }
matrix_c;
layout(set = 0, binding = 3) buffer Layout2A { Layout array[2]; }
layout_a;
layout(set = 0, binding = 4) buffer Layout2B { Layout array[2]; }
layout_b;
layout(set = 0, binding = 5) buffer Layout2C { Layout array[2]; }
layout_c;

void main() {
  uint idx = gl_GlobalInvocationID.x;
  uint idy = gl_GlobalInvocationID.y;
  if ((idy < layout_a.array[1].size) && (idx < layout_b.array[0].size)) {
    float sum = 0.0;
    // layout_a.array[0].size == layout_b.array[1].size
    for (uint i = 0; i < layout_a.array[0].size; ++i) {
      uint ida = layout_a.array[1].stride * idy + layout_a.array[0].stride * i;
      uint idb = layout_b.array[1].stride * i + layout_b.array[0].stride * idx;
      float a = matrix_a.data[ida];
      float b = matrix_b.data[idb];
      sum += a * b;
    }
    uint idc = layout_c.array[1].stride * idy + layout_c.array[0].stride * idx;
    matrix_c.data[idc] = sum;
  }
}
