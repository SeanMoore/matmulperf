SHELL:=/bin/bash
# .SHELLFLAGS:=-euo pipefail
.ONESHELL:

all: \
	bin/stl_slow.elf \
	bin/stl_fast.elf \
	bin/cpp_slow.elf \
	bin/cpp_fast.elf \
	bin/cf_slow.elf \
	bin/cf_fast.elf \
	bin/f_slow.elf \
	bin/f_fast.elf \
	bin/c_slow.elf \
	bin/c_fast.elf \
	bin/rv_fast.elf \
	bin/r_slow.elf \
	bin/r_fast.elf \
	bin/r_rayon_slow.elf \
	bin/r_rayon_fast.elf

bin:
	mkdir -p bin

bin/rv_fast.elf: rv | bin # We don't want insight into the Rust directory, just change if anything it that directory does.
	pushd rv
		cargo install --path . --root ..
	popd
	pushd bin
		mv rv rv_fast.elf
	popd

bin/r_slow.elf: r | bin # We don't want insight into the Rust directory, just change if anything it that directory does.
	pushd r
		cargo install --debug --path . --root ..
	popd
	pushd bin
		mv r r_slow.elf
	popd

bin/r_fast.elf: r | bin # We don't want insight into the Rust directory, just change if anything it that directory does.
	pushd r
		cargo install --path . --root ..
	popd
	pushd bin
		mv r r_fast.elf
	popd

bin/r_rayon_slow.elf: r_rayon | bin # We don't want insight into the Rust directory, just change if anything it that directory does.
	pushd r_rayon
		cargo install --debug --path . --root ..
	popd
	pushd bin
		mv r_rayon r_rayon_slow.elf
	popd

bin/r_rayon_fast.elf: r_rayon | bin # We don't want insight into the Rust directory, just change if anything it that directory does.
	pushd r_rayon
		cargo install --path . --root ..
	popd
	pushd bin
		mv r_rayon r_rayon_fast.elf
	popd

bin/stl_slow.elf: stl.cpp | bin
	g++ stl.cpp -std=c++17 $$(pkg-config tbb --libs) -o bin/stl_slow.elf

bin/stl_fast.elf: stl.cpp | bin
	g++ stl.cpp -std=c++17 $$(pkg-config tbb --libs) -march=native -Ofast -o bin/stl_fast.elf

bin/cpp_slow.elf: cpp.cpp | bin
	g++ cpp.cpp -o bin/cpp_slow.elf

bin/cpp_fast.elf: cpp.cpp | bin
	g++ cpp.cpp -march=native -Ofast -o bin/cpp_fast.elf

bin/f_slow.elf: f.f90 | bin
	gfortran f.f90 -o bin/f_slow.elf

bin/f_fast.elf: f.f90 | bin
	gfortran f.f90 -march=native -Ofast -o bin/f_fast.elf

bin/c_slow.elf: c.c | bin
	gcc c.c -o bin/c_slow.elf

bin/c_fast.elf: c.c | bin
	gcc c.c -march=native -Ofast -o bin/c_fast.elf

bin/cf_slow.elf: cf.c cf.f90 | bin
	gfortran -c cf.f90 -o bin/cf.slow.f.o
	gcc -c cf.c -o bin/cf.slow.c.o
	gfortran bin/cf.slow.f.o bin/cf.slow.c.o -o bin/cf_slow.elf

bin/cf_fast.elf: cf.c cf.f90 | bin
	gfortran -c cf.f90 -march=native -Ofast -o bin/cf.fast.f.o
	gcc -c cf.c -march=native -Ofast -o bin/cf.fast.c.o
	gfortran bin/cf.fast.f.o bin/cf.fast.c.o -o bin/cf_fast.elf

clean:
	-rm -f bin/*
	-rmdir bin