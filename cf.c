#include <stdio.h>
#include <stdlib.h>

#define DIMENSION 2000

#define AR DIMENSION
#define AC DIMENSION
#define BR AC
#define BC DIMENSION

typedef float Value;

Value A[AR][AC];
Value B[BC][BR];
Value C[AR][BC];

// a[i][k]
// b[k][j]
// c[i][j]
// c = matmul(a, b)
extern void fmatmul_( int* i, int* j, int* k, Value* a, Value* b, Value* c );

int main() {
    for( int r = 0; r < AR; ++r ) {
        for( int c = 0; c < AC; ++c ) {
            A[r][c] = r * c;
        }
    }

    for( int c = 0; c < BC; ++c ) {
        for( int r = 0; r < BR; ++r ) {
            B[c][r] = r * c;
        }
    }

    int i = AR, j = AC, k = BC;
    fmatmul_( &i, &j, &k,
              (Value*)A,
              (Value*)B,
              (Value*)C );

    Value sum = 0.0;
    for( int i = 0; i < AR; ++i ) {
        for( int j = 0; j < BC; ++j ) {
            sum += C[i][j];
        }
    }
    printf( "%f\n", sum );
}