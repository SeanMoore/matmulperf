#include <stdio.h>
#include <stdlib.h>

#define DIMENSION 2000

#define AR DIMENSION
#define AC DIMENSION
#define BR AC
#define BC DIMENSION

typedef float Value;

Value A[AR][AC];
Value B[BC][BR];
Value C[AR][BC];

int main() {
    for( int r = 0; r < AR; ++r ) {
        for( int c = 0; c < AC; ++c ) {
            A[r][c] = r * c;
        }
    }

    for( int c = 0; c < BC; ++c ) {
        for( int r = 0; r < BR; ++r ) {
            B[c][r] = r * c;
        }
    }

    for( int r = 0; r < AR; ++r ) {
        for( int c = 0; c < BC; ++c ) {
            Value sum = 0.0;
            for( int i = 0; i < AC; ++i ) {
                sum += A[r][i] * B[c][i];
            }
            C[r][c] = sum;
        }
    }

    Value sum = 0.0;
    for( int r = 0; r < AR; ++r ) {
        for( int c = 0; c < BC; ++c ) {
            sum += C[r][c];
        }
    }
    printf( "%f\n", sum );
}